﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalScore : MonoBehaviour {

    public double timePassed;
    private bool fsRun;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        fsRun = false;
        timePassed = 0;
    }

    private void Update()
    {
        if (fsRun)
        {
            timePassed += Time.deltaTime;
            Debug.Log(Mathf.Round((float)timePassed));
        }
    }

    public void StartRun()
    {
        fsRun = true;
    }

    public void StopRun()
    {
        fsRun = false;
    }
}
