﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HeroController : MonoBehaviour {

    public float speed;
    public GameManager gameManager;
    public LayerMask blockingLayer;
    public GameObject bloodScreen;
    public float bloodDiming = 0.1f;
    public bool isLocked = false;
    Animator anim;
    public GameObject lifeHeart;
    public int life;
    public float lifeGap;
    public Vector2 lifePosition;
    public AudioClip pain;
    public AudioClip footsteps;
    public GameObject keyOverlay;
    public Vector3 overlayRelativePosition;
    
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private bool isMoving = false;
    private Vector2 movement;
    private Vector2 target;
    private Vector2 prev;
    private float distance;
    private BoxCollider2D boxCollider;
    private AudioSource audio;
    private GameObject[] hearts;
    private GameObject bloodScreenInstance;
    private GameObject overlayInstance;
    private int touchingInteractive = 0;

    void Start()
    {
        this.rb = GetComponent<Rigidbody2D>();
        this.sr = GetComponentInChildren<SpriteRenderer>();
        this.anim = GetComponentInChildren<Animator>();
        this.boxCollider = GetComponent<BoxCollider2D>();
        this.audio = GetComponent<AudioSource>();
        this.audio.Play();
        this.audio.Pause();

        hearts = new GameObject[life];

        for (int i = 0; i < life; i++)
        {
            hearts[i] = Instantiate(lifeHeart);
            hearts[i].transform.position = new Vector2(lifePosition.x + i * lifeGap, lifePosition.y);
        }
    }

    public void takeDamage()
    {
        life--;
        Destroy(hearts[life]);
        Debug.Log("Take Damage");
        this.audio.clip = pain;
        this.audio.loop = false;
        this.audio.Play();        

        if (life <= 0)
        {
            die();
        }
        else
        {
            if (bloodScreenInstance != null)
            {
                SpriteRenderer bloodSR = bloodScreenInstance.GetComponent<SpriteRenderer>();
                bloodSR.color = new Color(bloodSR.color.r, bloodSR.color.g, bloodSR.color.b, 1.0f);
                
            }
            else
            {
                bloodScreenInstance = Instantiate(bloodScreen);
            }
        }
    }

    private void die()
    {
        gameManager.onGameOver(false);
    }

    bool isValidMove(Vector2 movement)
    {
        Vector2 pivot = rb.position + boxCollider.offset;

        boxCollider.enabled = false;
        RaycastHit2D hit = Physics2D.Linecast(pivot, pivot + movement, blockingLayer);
        boxCollider.enabled = true;

        if (hit.transform ==  null)
        {
            return true;
        }

        return false;
    }

    // Update is called once per frame
    void Update() {

        var x = Input.GetAxisRaw("Horizontal");
        var y = Input.GetAxisRaw("Vertical");
        

        if (bloodScreenInstance != null)
        {
            SpriteRenderer bloodSR = bloodScreenInstance.GetComponent<SpriteRenderer>();
            
            if (bloodSR.color.a > 0)
            {
                bloodSR.color = new Color(bloodSR.color.r, bloodSR.color.g, 
                    bloodSR.color.b, bloodSR.color.a - bloodDiming);
            }
        }

        // Update Key Overlay's Position
        if (overlayInstance != null)
        {
            overlayInstance.transform.position = transform.position + overlayRelativePosition;
        }

        if (!isMoving)
        {
            if (isLocked)
            {
                return;
            }

            if (Mathf.Approximately(x, 0.0f) && Mathf.Approximately(y, 0.0f))
                return;

            if (Mathf.Abs(x) > Mathf.Abs(y))
                movement = Mathf.Sign(x) * new Vector2(1.0f, 0.0f);
            else
                movement = Mathf.Sign(y) * new Vector2(0.0f, 1.0f);

            if (!isValidMove(movement))
            {
                isMoving = false;
                this.anim.SetBool("playerWalk", false);
                return;
            }

            prev = rb.position;
            target = movement + rb.position;
            //Debug.Log("Position: " + rb.position);
            //Debug.Log("Target: " + target);
            rb.velocity = speed * movement;
            isMoving = true;
            distance = 0.0f;
            this.anim.SetBool("playerWalk", true);

            if (manageAudioClip())
                this.audio.UnPause();

            if (Mathf.Approximately(movement.x, 1.0f)) 
            {
                sr.flipX = false;
            }
            else if (Mathf.Approximately(movement.x, -1.0f))
            {
                sr.flipX = true;
            }
        }

        if (isMoving)
        {
            if (isLocked)
            {
                rb.velocity = Vector2.zero;
                isMoving = false;
                this.anim.SetBool("playerWalk", false);
            
                return;
            }

            distance += (rb.position - prev).magnitude;
            prev = rb.position;

            if (distance >= 1.0f)
            {
                transform.position = target;
                rb.velocity = Vector2.zero;
                isMoving = false;
                if (this.audio.clip == footsteps)
                    this.audio.Pause();
                var input = new Vector2(x, y);
                if (input.sqrMagnitude == 0.0)
                    this.anim.SetBool("playerWalk", false);
            }

        }

	}

    private bool manageAudioClip()
    {
        if (this.audio.clip == footsteps) return true;
        else if (this.audio.isPlaying) return false;
        else
        {
            this.audio.clip = footsteps;
            this.audio.loop = true;
            this.audio.Play();
            return true;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Interactive"))
        {

            Debug.Log("Interactive Trigger Enter");
            touchingInteractive++;

            // Create Key Overlay Instance
            if (overlayInstance == null)
            {
                overlayInstance = Instantiate(keyOverlay);
                overlayInstance.transform.position = transform.position + overlayRelativePosition;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Interactive"))
        {
            Debug.Log("Interactive Trigger Exit");
            touchingInteractive--;

            // Destroy Key Overlay
            if (overlayInstance != null && touchingInteractive <= 0)
            {
                Destroy(overlayInstance);
            }
        }
    }
}
