﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartToMenuAfterSeconds : MonoBehaviour {

    public float seconds = 5.0f;

	// Use this for initialization
	void Start () {
        StartCoroutine("ToMenu");
    }

    // Update is called once per frame
    IEnumerator ToMenu()
    {
        yield return new WaitForSeconds(seconds);
        var finalScore = GameObject.FindGameObjectWithTag("ScoreHolder");        
        SceneManager.LoadScene("MainMenu");
        //finalScore.GetComponent<FinalScore>().timePassed = 0.0f;
        Destroy(finalScore);
    }

}
