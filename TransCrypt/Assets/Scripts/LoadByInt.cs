﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadByInt : MonoBehaviour {

    public int scene; 

    public void LoadScene()
    {
        SceneManager.LoadScene(scene);
    }

}
