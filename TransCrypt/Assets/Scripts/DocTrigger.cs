﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocTrigger : MonoBehaviour {

    public GameManager gameManager;
    public GameObject closeButton;
    public GameObject background;
    public GameObject[] documents;
    public LayerMask playerLayer;
    public Vector2 closePosition;

    private BoxCollider2D boxCollider;
    private AudioSource audio;

    private static GameObject backgroundInstance;
    private GameObject buttonInstance;
    private bool inContact;
    private GameObject documentInstance;
    private int page = 0;

    private bool isOpened = false;

	// Use this for initialization
	void Start () {
        boxCollider = GetComponent<BoxCollider2D>();
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.E))
        {
            //if (boxCollider.IsTouchingLayers(playerLayer))
            if (inContact)
            {

                if (isOpened)
                {
                    Debug.Log("Close Book");
                    closeView();
                }
                else
                {
                    Debug.Log("Open Book");
                    openView();
                }              
            }
        }

        if (isOpened)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                page = --page % documents.Length;
                Destroy(documentInstance);
                openView();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                page = ++page % documents.Length;
                Destroy(documentInstance);
                openView();
            }
        }
        


    }

    public void openView()
    {
        if (backgroundInstance == null)
        {
            backgroundInstance = Instantiate(background);

        }

        gameManager.lockPlayer(true);

        if (documents.Length > 0)
        {
            documentInstance = Instantiate(documents[page], new Vector3(0.0f, 0.0f, -3.0f), Quaternion.identity);
            audio.Play();
        }

        isOpened = true;
    }

    public void closeView()
    {
        Destroy(backgroundInstance);

        gameManager.lockPlayer(false);

        if (documentInstance != null) audio.Play();

        Destroy(documentInstance);
        documentInstance = null;

        isOpened = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            inContact = true;
            Debug.Log(gameObject.name + ": enter (" + collision.name + ")");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            inContact = false;
            Debug.Log(gameObject.name + ": exit (" + collision.name + ")");
        }
    }
}
