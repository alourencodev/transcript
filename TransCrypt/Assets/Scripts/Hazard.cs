﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{

    private bool isInside = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.isTrigger)
        {
            isInside = !isInside;

            if (isInside)
            {
                collision.gameObject.GetComponent<HeroController>().takeDamage();
                Debug.Log("Trigger Enter");
            }
        }
    }
}
