﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statue : MonoBehaviour
{

    public Sprite[] directionSprites;
    public int rotation = 0;

    // UP = 0;
    // RIGHT = 1;
    // DOWN = 2;
    // LEFT = 3;

    private SpriteRenderer sr;

    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = directionSprites[rotation];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void applyRotation(int angle)
    {
        rotation += angle;

        if (rotation >= 4)
        {
            rotation -= 4;
        }

        sr.sprite = directionSprites[rotation];
    }

    public int getRotation()
    {
        return rotation;
    }
}
