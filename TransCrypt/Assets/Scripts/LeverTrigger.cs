﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverTrigger : MonoBehaviour {

    private bool inContact;

    private SpriteRenderer sp;
    private AudioSource audio;
    public Sprite spbOn, spbOff, spwOn, spwOff;
    public bool toggleState;

    // Use this for initialization
    void Start () {
        audio = GetComponentInParent<AudioSource>();
        toggleState = false;
        sp = this.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyDown(KeyCode.E))
        {
            if(inContact)
            {
                audio.Play();
                toggleState = true;
                ChangeState();
            }
        }

	}
    
    private void ChangeState()
    {
        if (sp.sprite == spbOn)
        {
            sp.sprite = spbOff;
            toggleState = false;
            return;
        }

        if (sp.sprite == spbOff)
        {
            sp.sprite = spbOn;
            toggleState = true;
            return;
        }

        if (sp.sprite == spwOn)
        {
            sp.sprite = spwOff;
            toggleState = false;
            return;
        }

        if (sp.sprite == spwOff)
        {
            sp.sprite = spwOn;
            toggleState = true;
            return;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            inContact = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            inContact = false;
        }
    }
}
