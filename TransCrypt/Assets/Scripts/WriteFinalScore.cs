﻿using UnityEngine;
using UnityEngine.UI;

public class WriteFinalScore : MonoBehaviour {

    [HideInInspector]
    public GameObject finalScoreHolder;
    [HideInInspector]
    public Text finalText;

    private void Start()
    {
        finalText = this.GetComponent<Text>();
        finalScoreHolder = GameObject.FindGameObjectWithTag("ScoreHolder");
    }

    private void Update()
    {
        finalScoreHolder.GetComponent<FinalScore>().StopRun();
        finalText.text = "Final Score:" +
            System.Math.Round(finalScoreHolder.GetComponent<FinalScore>().timePassed,3)
            + " seconds";
    }
}
