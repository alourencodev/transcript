﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomTrigger : MonoBehaviour {

    public string goToRoom;
    private AudioSource audio;

	// Use this for initialization
	void Start () {
        this.audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && !collision.isTrigger)
        {
            Debug.Log(gameObject.name + ": enter (" + collision.name + ")");
            StartCoroutine("EndLevel");
            //this.audio.Play();
            //SceneManager.LoadScene(goToRoom);
        }
    }

    private IEnumerator EndLevel()
    {
        this.audio.Play();
        yield return new WaitForSeconds(this.audio.clip.length);
        SceneManager.LoadScene(goToRoom);
    }
}
