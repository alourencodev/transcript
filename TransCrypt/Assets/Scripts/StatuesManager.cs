﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatuesManager : MonoBehaviour {

    public GameManager gameManager;
    public GameObject treasure;
    public Sprite treasureSprite;
    public Vector3 treasurePosition;
    public PressurePlates[] pressurePlates;
    public Statue[] statues;
    public int[] solution;
    private bool solved = false;

    private GameObject treasureInstance;

	// Use this for initialization
	void Start () {
		
	}
	


	// Update is called once per frame
	void Update () {
        if (!solved)
        {
            for (int i = 0; i < statues.Length; i++)
            {
                if (statues[i].getRotation() != solution[i])
                {
                    return;
                }
            }

            solved = true;
            GetComponent<AudioSource>().Play();
            treasureInstance = Instantiate(treasure);
            treasureInstance.transform.position = treasurePosition;
            treasureInstance.GetComponent<Treasure>().gameManager = gameManager;

            for (int i = 0; i < pressurePlates.Length; i++)
            {
                pressurePlates[i].isActive = false;
            }
        }
	}

    
}
