﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlates : MonoBehaviour {

    public Statue[] statues;
    public int rotation;
    public bool isActive = true;

    private bool isOnTop = false;
    private AudioSource audio;

	// Use this for initialization
	void Start () {
        this.audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isActive)
        {
            return;
        }

        if (!collision.isTrigger)
        {
            isOnTop = !isOnTop;

            if (isOnTop)
            {
                audio.Play();

                for (int i = 0; i < statues.Length; i++)
                {
                    statues[i].applyRotation(rotation);
                }
            }
        }
    }
}
