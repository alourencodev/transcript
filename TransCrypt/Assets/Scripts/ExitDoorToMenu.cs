﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitDoorToMenu : MonoBehaviour {

    bool inContact;
    int count = 0;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (inContact)
            {
                var finalScore = GameObject.FindGameObjectWithTag("ScoreHolder");
                Destroy(finalScore);
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ++count;
            inContact = true;
            Debug.Log(gameObject.name + ": enter (" + collision.name + ")");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            --count;
            if (count == 0) inContact = false;
            Debug.Log(gameObject.name + ": exit (" + collision.name + ")");
        }
    }
}
