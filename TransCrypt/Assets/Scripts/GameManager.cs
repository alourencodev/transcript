﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public GameObject player;
    public GameObject foe;
    public GameObject map;

    public Vector3 playerSpawn;

    public LayerMask playerLayer;
    public LayerMask objectsLayer;

    public string endLostScene;
    public string endWinScene;

    private GameObject playerInstance;

    public GameObject getPlayer()
    {
        return player;
    }

    // Use this for initialization
    void Start()
    {
        playerInstance = Instantiate(player);
        playerInstance.transform.position = playerSpawn;
        playerInstance.GetComponent<HeroController>().gameManager = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void lockPlayer(bool isLocked)
    {
        playerInstance.GetComponent<HeroController>().isLocked = isLocked;
    }

    public void onGameOver(bool hasWon)
    {
        Destroy(playerInstance);

        if (hasWon)
        {
            SceneManager.LoadScene(endWinScene);
        }
        else
        {
            SceneManager.LoadScene(endLostScene);
        }
        
    }

    public void onLevelWin()
    {

    }
}
