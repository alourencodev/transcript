﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorTrigger : MonoBehaviour
{

    public GameManager gameManager;
    public string nextLevel;

    private GameObject player;
    private AudioSource audio;
    private bool isColliding = false;
    private int lifeTaken = 0;      // HARDCODED AS FUCK!!!!

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        audio = GetComponentInParent<AudioSource>();
    }

    private void goToNext()
    {
        StartCoroutine("EndLevel");
        //audio.Play();
        //SceneManager.LoadScene(nextLevel);
    }

    private IEnumerator EndLevel()
    {
        this.audio.Play();
        yield return new WaitForSeconds(this.audio.clip.length);
        SceneManager.LoadScene(nextLevel);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = GameObject.FindGameObjectWithTag("Player");

        if (collision.isTrigger)
        {
            return;
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            if (ConfigLoader.canLeave)
            {
                if (ConfigLoader.solSet[3] == 1 && this.name == "DoorN")
                {
                    Debug.Log("Left the room through N");
                    goToNext();
                }
                else if (ConfigLoader.solSet[3] == 2 && this.name == "DoorE")
                {
                    Debug.Log("Left the room through E");
                    goToNext();

                }
                else if (ConfigLoader.solSet[3] == 3 && this.name == "DoorS")
                {
                    Debug.Log("Left the room through S");
                    goToNext();

                }
                else if (ConfigLoader.solSet[3] == 4 && this.name == "DoorW")
                {
                    Debug.Log("Left the room through W");
                    goToNext();

                }
                else
                {
                    Debug.Log("Wrong door");
                    player.gameObject.SetActive(false);
                    player.transform.position = gameManager.playerSpawn;
                    player.gameObject.SetActive(true);

                    if (lifeTaken == 1)
                    {
                        player.GetComponent<HeroController>().takeDamage();
                        isColliding = true;
                        lifeTaken = 0;
                    }
                    else
                    {
                        lifeTaken++;
                    }
                }
            }
            else
            {
                Debug.Log("Can't Leave");
                player.gameObject.SetActive(false);
                player.transform.position = gameManager.playerSpawn;
                player.gameObject.SetActive(true);

                if (lifeTaken == 1)
                {
                    player.GetComponent<HeroController>().takeDamage();
                    isColliding = true;
                    lifeTaken = 0;
                }
                else
                {
                    lifeTaken++;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
    }
}
