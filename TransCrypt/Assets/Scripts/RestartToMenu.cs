﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("ToMenu");
	}
	
	// Update is called once per frame
	IEnumerator ToMenu() {
        AudioSource audio = GetComponent<AudioSource>();
        yield return new WaitForSeconds(audio.clip.length);
        var finalScore = GameObject.FindGameObjectWithTag("ScoreHolder");
        //finalScore.GetComponent<FinalScore>().timePassed = 0.0f;
        Destroy(finalScore);
        SceneManager.LoadScene("MainMenu");
	}
}
