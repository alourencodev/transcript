﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foe : MonoBehaviour
{

    public float speed;

    private GameObject target;
    private Rigidbody rb;

    public void setTarget(GameObject target)
    {
        this.target = target;
    }

    // Use this for initialization
    void Start()
    {
       rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = target.transform.position - transform.position;
        transform.position += direction.normalized * speed;
    }
}
