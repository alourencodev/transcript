﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

    public double _timeLeft;
    public Slider timeBar;
    public GameObject timeBarFill;
    
    [System.NonSerialized]
    public Image fillBar;
    [System.NonSerialized]
    public bool tRun;

    private void Start()
    {
        fillBar = timeBarFill.GetComponent<Image>();
        timeBar.gameObject.SetActive(false);
        tRun = false;
        timeBar.value = (float)_timeLeft;
        StartTimer();
    }

    private void Update()
    {
        if (tRun)
        {
            timeBar.gameObject.SetActive(true);
            _timeLeft -= Time.deltaTime;
            timeBar.value = (float)_timeLeft;
            fillBar.color = Color.HSVToRGB(Mathf.Lerp(0, 0.334f, (float)_timeLeft/10), 1, 1);
            if (_timeLeft <= 0)
            {
                StopTimer();
                this.gameObject.GetComponent<GameManager>().onGameOver(false);
            }
        }
    }

    public void StopTimer()
    {
        tRun = false;
        timeBar.gameObject.SetActive(false);
    }

    public void StartTimer() {
        tRun = true;
    }

    public void RestartTimer() {
        _timeLeft = 60;
        tRun = false;
    }

    public double GetTimeLeft()
    {
        return _timeLeft;
    }
}