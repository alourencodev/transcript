﻿using UnityEngine;
using System.Linq;

public class ConfigLoader : MonoBehaviour {

    [HideInInspector]
    public static int[] cProb;
    [HideInInspector]
    public static int[] solSet;
    [HideInInspector]
    public static int[] currSet;

    public static bool canLeave;

    public GameObject[] levers;
    private SpriteRenderer[] leverSprites;
    public Sprite sprite1, sprite2, sprite3;

	// Use this for initialization
	void Start () {

        canLeave = false;
        leverSprites = new SpriteRenderer[3];
        solSet = new int[] { -1, -1, -1, -1 };
        currSet = new int[] { -1, -1, -1, -1 };

        int i = 0;
        for (i = 0; i<= 2; i++)
        {
            leverSprites[i] = levers[i].GetComponent<SpriteRenderer>();
        }

        PickRandomProb();

        solSet = GenSolution();

        Debug.Log("Solution:" + "" + solSet[0] + " " + " " + solSet[1] + " " + " " + solSet[2] + " " + " " + solSet[3]);
    }

    private void Update()
    {
        CheckSolution();
    }

    private void PickRandomProb()
    {
        int i = 0;

        do
        {
            cProb = new int[] { Random.Range(0, 3), Random.Range(0, 3), Random.Range(0, 3) };
        } while (cProb.SequenceEqual(new int[] { 0, 0, 0 }));


        for(i = 0; i <= 2;i++)
        {
            switch (cProb[i])
            {
                case 0:
                    // No Lever
                    leverSprites[i].sprite = sprite1;
                    leverSprites[i].gameObject.tag = "Untagged";
                    break;

                case 1:
                    // White
                    leverSprites[i].sprite = sprite2;
                    break;

                case 2:
                    // Black
                    leverSprites[i].sprite = sprite3;
                    break;
            }
        }
    }

    private int[] GenSolution()
    {
        //0 -> Empty, 1 -> White, 2-> Black

        //se _B_ -> Do nothing -> Leave right
        if (cProb[1] == 2)
        {
            return new int[] { 0, 0, 0, 2 };
        }

        // all w w_ _, _w_, _ _w, WW_, _WW, W_W, WWW t(all) l (N)
        else if ((cProb[0] == 0 || cProb[0] == 1)
                && (cProb[1] == 0 || cProb[1] == 1)
                && (cProb[2] == 0 || cProb[2] == 1)
                && (cProb[0] + cProb[1] + cProb[2] != 0))
        {
            return new int[] { cProb[0], cProb[1], cProb[2], 1 };
        }

        // 2 w  t(e,d) l(N)
        else if ((cProb[0] == 1 && cProb[1] == 1)
            || (cProb[0] == 1 && cProb[2] == 1)
            || (cProb[1] == 1 && cProb[2] == 1))
        {
            return new int[] { 0 < cProb[0] ? 1 : 0, 0, 0 < cProb[2] ? 1 : 0, 1 };
        }

        // B__ t(e) l(r)
        else if(cProb[0] == 2)
        {
            return new int[] { 1,0,0,4};
        }

        //Default
        else
        {
            return new int[] { 0, 0, 0 < cProb[2] ? 1 : 0, 2 };
        }
    }

    private void CheckSolution()
    {
        currSet[0] = (levers[0].GetComponent<LeverTrigger>().toggleState ? 1 : 0);
        currSet[1] = (levers[1].GetComponent<LeverTrigger>().toggleState ? 1 : 0);
        currSet[2] = (levers[2].GetComponent<LeverTrigger>().toggleState ? 1 : 0);

        if(currSet[0] == solSet[0] && currSet[1] == solSet[1] && currSet[2] == solSet[2])
        {
            canLeave = true;
            Debug.Log("Go to door");
        }
        else
        {
            canLeave = false;
        }
    }
}
